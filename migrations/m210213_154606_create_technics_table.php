<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210213_154606_create_technics_table`.
 */
class m210213_154606_create_technics_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('technics', [
            'id' => $this->primaryKey(),
            'status' => $this->string()->comment('Статус'),
            'customer_id' => $this->integer()->comment('Клиент'),
            'full_name_id' => $this->integer()->comment('ФИО'),
            'telephone' => $this->string()->comment('Телефон'),
            'accepted_id' => $this->integer()->comment('Принят'),
            'executor_id' => $this->integer()->comment('Исполнитель'),
            'accepted_date' => $this->dateTime()->comment('Принят'),
            'technics_type_id' => $this->string()->comment('Техника'),
            'numbers' => $this->string()->comment('Номера'),
            'problems' => $this->string()->comment('Проблема'),

            'comment' => $this->string()->comment('Комментарий'),

            'payment' => $this->string()->comment('Расчет'),

            'material_price' => $this->string()->comment('Цена материала'),
            'price_of_work' => $this->string()->comment('Цена работы'),
            'sum' => $this->string()->comment('Сумма'),
        ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropTable('technics');
    }
}
