<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210213_154406_create_applications_table`.
 */
class m210213_154406_create_applications_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('applications', [
            'id' => $this->primaryKey(),
            'status' => $this->string()->comment('Статус'),
            'customer_id' => $this->string()->comment('Клиент'),
            'full_name_id' => $this->integer()->comment('ФИО'),
            'phone' => $this->string()->comment('Телефон'),
            'request' => $this->string()->comment('Заявка'),
            'date_of_adoption' => $this->datetime()->comment('Дата принятия'),
            'date_of_completion' => $this->datetime()->comment('Дата выполнения'),
            'date_of_payment' => $this->datetime()->comment('Дата оплаты'),
            'accepted_id' => $this->integer()->comment('Принял'),
            'fulfilled_id' => $this->integer()->comment('Выполнил'),
            'comment' => $this->string()->comment('Комментарий'),
            'sum' => $this->string()->comment('Сумма'),
            'payment' => $this->string()->comment('Расчет'),
            'telephone' => $this->string()->comment('Телефон'),
            'material_price' => $this->string()->comment('Цена материала'),
            'price_of_work' => $this->string()->comment('Цена работы'),
        ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropTable('applications');
    }
}
