<?php

use yii\db\Migration;

/**
 * Handles the creation of table `materials`.
 */
class m210214_123040_create_materials_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('materials', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'price' => $this->integer()->comment('Цена'),
            'shtrih' => $this->string()->comment('Штрих')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('materials');
    }
}
