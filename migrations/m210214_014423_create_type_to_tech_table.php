<?php

use yii\db\Migration;

/**
 * Handles the creation of table `type_t_tech`.
 */
class m210214_014423_create_type_to_tech_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('type_to_tech', [
            'id' => $this->primaryKey(),
            'technics_type_id' => $this->integer(),
            'technics_id' => $this->integer()

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('type_t_tech');
    }
}
