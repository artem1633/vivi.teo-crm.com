<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210213_154306_create_customer_table`.
 */
class m210213_154306_create_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'bin' => $this->string()->comment('Бин'),
            'telephone' => $this->string()->comment('Телефон'),
            'contract' => $this->string()->comment('Договор'),
            'payment' => $this->string()->comment('Расчет'),
            'valid_to' => $this->datetime()->comment('Действителен до'),
            'black_list' => $this->string()->comment('Черный список'),
            'comment' => $this->string()->comment('Комментарий'),
        ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropTable('customer');
    }
}
