<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210213_153906_create_toner_table`.
 */
class m210213_153906_create_toner_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('toner', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropTable('toner');
    }
}
