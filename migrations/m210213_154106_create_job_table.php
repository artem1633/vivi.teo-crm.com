<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210213_154106_create_job_table`.
 */
class m210213_154106_create_job_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('job', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'price' => $this->integer()->comment('Стоимость'),

        ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropTable('job');
    }
}
