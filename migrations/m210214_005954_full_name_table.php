<?php

use yii\db\Migration;

/**
 * Class m210214_005954_full_name_table
 */
class m210214_005954_full_name_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('full_name', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Ф.И.О.'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('full_name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210214_005954_full_name_table cannot be reverted.\n";

        return false;
    }
    */
}
