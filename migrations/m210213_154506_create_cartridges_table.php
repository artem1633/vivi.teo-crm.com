<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210213_154506_create_cartridges_table`.
 */
class m210213_154506_create_cartridges_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cartridges', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->comment('Клиент'),
            'accepted_id' => $this->integer()->comment('Принял'),
            'fulfilled_id' => $this->integer()->comment('Выполнил'),
            'accepted_date' => $this->dateTime()->comment('Принят'),
            'job_id' => $this->integer()->comment('Работа'),
            'given_away' => $this->string()->comment('Отдан'),

            'full_name_id' => $this->integer()->comment('ФИО'),
            'cartridge_id' => $this->integer()->comment('Картридж'),

            'comments' => $this->string()->comment('Комментарий'),
            'sum' => $this->string()->comment('Сумма'),
            'payment' => $this->string()->comment('Расчет'),
            'status' => $this->string()->comment('Статус'),
            'toner_id' => $this->integer()->comment('Тонер'),
            'toner_quantity' => $this->string()->comment('Количество тонера'),
            'drum_id' => $this->integer()->comment('Фотобарабан'),
            'number_of_imaging_drums' => $this->string()->comment('Количество фотобарабанов'),
        ]);

        $this->createIndex(
            'idx-cartridges-job_id',
            'cartridges',
            'job_id'
        );
                        
        $this->addForeignKey(
            'fk-cartridges-job_id',
            'cartridges',
            'job_id',
            'job',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-cartridges-cartridge_id',
            'cartridges',
            'cartridge_id'
        );
                        
        $this->addForeignKey(
            'fk-cartridges-cartridge_id',
            'cartridges',
            'cartridge_id',
            'cartridge',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-cartridges-toner_id',
            'cartridges',
            'toner_id'
        );
                        
        $this->addForeignKey(
            'fk-cartridges-toner_id',
            'cartridges',
            'toner_id',
            'toner',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-cartridges-drum_id',
            'cartridges',
            'drum_id'
        );
                        
        $this->addForeignKey(
            'fk-cartridges-drum_id',
            'cartridges',
            'drum_id',
            'photoconductor',
            'id',
            'SET NULL'
        );
                        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-cartridges-job_id',
            'cartridges'
        );
                        
        $this->dropIndex(
            'idx-cartridges-job_id',
            'cartridges'
        );
                        
                        $this->dropForeignKey(
            'fk-cartridges-cartridge_id',
            'cartridges'
        );
                        
        $this->dropIndex(
            'idx-cartridges-cartridge_id',
            'cartridges'
        );
                        
                        $this->dropForeignKey(
            'fk-cartridges-toner_id',
            'cartridges'
        );
                        
        $this->dropIndex(
            'idx-cartridges-toner_id',
            'cartridges'
        );
                        
                        $this->dropForeignKey(
            'fk-cartridges-drum_id',
            'cartridges'
        );
                        
        $this->dropIndex(
            'idx-cartridges-drum_id',
            'cartridges'
        );
                        
                        
        $this->dropTable('cartridges');
    }
}
