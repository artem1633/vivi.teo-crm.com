<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210213_154206_create_cartridge_table`.
 */
class m210213_154206_create_cartridge_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cartridge', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'price' => $this->integer()->comment('Стоимость'),
            'toner_id' => $this->integer()->comment('Тонер'),
            'photoconductor_id' => $this->integer()->comment('Фотобарабан')
        ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropTable('cartridge');
    }
}
