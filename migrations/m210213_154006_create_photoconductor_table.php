<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210213_154006_create_photoconductor_table`.
 */
class m210213_154006_create_photoconductor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('photoconductor', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropTable('photoconductor');
    }
}
