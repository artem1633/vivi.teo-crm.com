<?php

use yii\db\Migration;

/**
 * Handles the creation of table `technics_type`.
 */
class m210214_014205_create_technics_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('technics_type', [
            'id' => $this->primaryKey(),
            'name'  => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('technics_type');
    }
}
