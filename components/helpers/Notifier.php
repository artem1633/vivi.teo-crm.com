<?php

namespace app\components\helpers;

use app\models\Notification;
use app\models\User;
use Yii;


/**
 * Class Notifier
 * @package app\components\helpers
 */
class Notifier
{
    /**
     * @param User|User[] $to
     * @param string $subject
     * @param string $text
     */
    public static function notify($to, $subject, $text)
    {
        if(is_array($to)){
            foreach ($to as $t){
                self::send($t, $subject, $text);
            }
        } else {
            self::send($to, $subject, $text);
        }
    }

    /**
     * @param User|User[] $to
     * @param string $subject
     * @param string $text
     */
    private static function send($to, $subject, $text)
    {
        try {
            (new Notification([
                'user_id' => $to->id,
                'subject' => $subject,
                'text' => $text
            ]))->save(false);

            Yii::$app->mailer->compose()
                ->setTo($to->login)
                ->setFrom('hh.notify@yandex.ru')
                ->setSubject($subject)
                ->setHtmlBody($text)
                ->send();
        } catch (\Exception $e)
        {
            Yii::warning('Email sending failed');
        }
    }
}