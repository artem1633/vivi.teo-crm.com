<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "materials".
 *
 * @property int $id
 * @property string $name Название
 * @property int $price Цена
 * @property string $shtrih Штрих
 */
class Materials extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'materials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price'], 'integer'],
            [['name', 'shtrih'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'price' => 'Цена',
            'shtrih' => 'Штрих',
        ];
    }

    /**
     * @inheritdoc
     * @return MaterialsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MaterialsQuery(get_called_class());
    }
}
