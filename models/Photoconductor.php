<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "photoconductor".
*
    * @property string $name Наименование
*/
class Photoconductor extends \yii\db\ActiveRecord
{

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'photoconductor';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => 'Наименование',
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        return parent::beforeSave($insert);
    }


        

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getCartridgess()
    {
        return $this->hasMany(Cartridges::className(), ['drum_id' => 'id']);
    }

}