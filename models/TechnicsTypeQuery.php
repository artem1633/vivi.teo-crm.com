<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TechnicsType]].
 *
 * @see TechnicsType
 */
class TechnicsTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TechnicsType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TechnicsType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
