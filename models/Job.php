<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "job".
*
    * @property string $name Наименование
*/
class Job extends \yii\db\ActiveRecord
{

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'job';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['price'],'integer']
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => 'Наименование',
            'price' => 'Стоимость',
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        return parent::beforeSave($insert);
    }


        

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getCartridgess()
    {
        return $this->hasMany(Cartridges::className(), ['job_id' => 'id']);
    }

}