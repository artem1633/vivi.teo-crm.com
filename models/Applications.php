<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "applications".
 *
 * @property int $id
 * @property string $status Статус
 * @property string $customer_id Клиент
 * @property int $full_name_id ФИО
 * @property string $phone Телефон
 * @property string $request Заявка
 * @property string $date_of_adoption Дата принятия
 * @property string $date_of_completion Дата выполнения
 * @property string $date_of_payment Дата оплаты
 * @property int $accepted_id Принял
 * @property int $fulfilled_id Выполнил
 * @property string $comment Комментарий
 * @property string $sum Сумма
 * @property string $payment Расчет
 * @property string $telephone Телефон
 * @property string $material_price Цена материала
 * @property string $price_of_work Цена работы
 */
class Applications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'applications';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'date_of_adoption',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['accepted_id', 'fulfilled_id' ,'customer_id'], 'integer'],
            ['full_name_id', 'validateClient'],
            [['full_name_id'],'required', 'when' => function ($model) {
             return $model->customer_id == 1;
            }],
            [['date_of_adoption', 'date_of_completion', 'date_of_payment','full_name_id' ], 'safe'],
            [['status', 'phone', 'request', 'comment', 'sum', 'payment', 'telephone', 'material_price', 'price_of_work'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'customer_id' => 'Клиент',
            'full_name_id' => 'ФИО',
            'phone' => 'Телефон',
            'request' => 'Заявка',
            'date_of_adoption' => 'Дата принятия',
            'date_of_completion' => 'Дата выполнения',
            'date_of_payment' => 'Дата оплаты',
            'accepted_id' => 'Принял',
            'fulfilled_id' => 'Выполнил',
            'comment' => 'Комментарий',
            'sum' => 'Сумма',
            'payment' => 'Расчет',
            'telephone' => 'Телефон',
            'material_price' => 'Цена материала',
            'price_of_work' => 'Цена работы',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFull_name()
    {
        return $this->hasOne(FullName::className(), ['id' => 'full_name_id']);
    }

    public function validateClient($attribute, $params)
    {
        $client = FullName::find()->where(['id' => $this->full_name_id])->one();
        if (!isset($client))
        {
            $client = new FullName();
            $client->name = $this->full_name_id;
            $error = $client->errors;

            if ($client->save())
            {
                $this->full_name_id = $client->id;
            }
            else
            {
                $this->addError($attribute,"Не создан новый клиент");
            }
        }
    }

    /**
     * @inheritdoc
     * @return ApplicationsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplicationsQuery(get_called_class());
    }
}
