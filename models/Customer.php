<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "customer".
*
    * @property string $name Наименование
    * @property string $bin Бин
    * @property string $telephone Телефон
    * @property string $contract Договор
    * @property string $payment Расчет
    * @property  $valid_to Действителен до
    * @property string $black_list Черный список
    * @property string $comment Комментарий
*/
class Customer extends \yii\db\ActiveRecord
{

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'customer';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name', 'bin', 'telephone', 'contract', 'payment', 'black_list', 'comment'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => 'Наименование',
            'bin' => 'Бин',
            'telephone' => 'Телефон',
            'contract' => 'Договор',
            'payment' => 'Расчет',
            'valid_to' => 'Действителен до',
            'black_list' => 'Черный список',
            'comment' => 'Комментарий',
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        return parent::beforeSave($insert);
    }


    
    
    
    
    
    
    
        
}