<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type_to_tech".
 *
 * @property int $id
 * @property int $technics_type_id
 * @property int $technics_id
 */
class TypeToTech extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_to_tech';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['technics_type_id', 'technics_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'technics_type_id' => 'Technics Type ID',
            'technics_id' => 'Technics ID',
        ];
    }

    /**
     * @inheritdoc
     * @return TypeToTechQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TypeToTechQuery(get_called_class());
    }
}
