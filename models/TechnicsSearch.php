<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Technics;

/**
 * TechnicsSearch represents the model behind the search form about `app\models\Technics`.
 */
class TechnicsSearch extends Technics
{
    public $search;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'full_name_id', 'accepted_id', 'executor_id'], 'integer'],
            [['status','search', 'telephone', 'accepted_date', 'technics_type_id', 'numbers', 'problems', 'comment', 'payment', 'material_price', 'price_of_work', 'sum'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Technics::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'full_name_id' => $this->full_name_id,
            'accepted_id' => $this->accepted_id,
            'executor_id' => $this->executor_id,
            'accepted_date' => $this->accepted_date,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'technics_type_id', $this->technics_type_id])
            ->andFilterWhere(['like', 'numbers', $this->numbers])
            ->andFilterWhere(['like', 'problems', $this->problems])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'payment', $this->payment])
            ->andFilterWhere(['like', 'material_price', $this->material_price])
            ->andFilterWhere(['like', 'price_of_work', $this->price_of_work])
            ->andFilterWhere(['like', 'sum', $this->sum]);

        $query->orFilterWhere(['like', 'technics.telephone', $this->search])
            ->orFilterWhere(['like', 'numbers', $this->search]);
        $query->joinWith(['full_name', 'customer']);
        $query
            ->orFilterWhere(['like', 'full_name.name', $this->search])
//            ->orFilterWhere(['like', 'numbers', $this->search])
//            ->orFilterWhere(['like', 'telephone', $this->search])
            ->orFilterWhere(['like', 'customer.name', $this->search]);

        return $dataProvider;
    }
}
