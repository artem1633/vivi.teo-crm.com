<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Technics]].
 *
 * @see Technics
 */
class TechnicsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Technics[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Technics|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
