<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "cartridge".
*
    * @property string $name Наименование
*/
class Cartridge extends \yii\db\ActiveRecord
{

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'cartridge';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['price','toner_id','photoconductor_id'],'integer']
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => 'Наименование',
            'price' => 'Стоимость',
            'toner_id' => 'Тонер',
            'photoconductor_id' => 'Фотобарабан',
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        return parent::beforeSave($insert);
    }


        

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getCartridgess()
    {
        return $this->hasMany(Cartridges::className(), ['cartridge_id' => 'id']);
    }

}