<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Cartridges]].
 *
 * @see Cartridges
 */
class CartridgesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Cartridges[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Cartridges|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
