<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "technics_type".
 *
 * @property int $id
 * @property string $name
 */
class TechnicsType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'technics_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * @inheritdoc
     * @return TechnicsTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TechnicsTypeQuery(get_called_class());
    }
}
