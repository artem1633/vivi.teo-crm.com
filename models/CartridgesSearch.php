<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cartridges;

/**
 * CartridgesSearch represents the model behind the search form about `app\models\Cartridges`.
 */
class CartridgesSearch extends Cartridges
{
    public $search;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'accepted_id', 'fulfilled_id', 'job_id', 'full_name_id', 'cartridge_id', 'toner_id', 'drum_id'], 'integer'],
            [['accepted_date','search',  'given_away', 'comments', 'sum', 'payment', 'status', 'toner_quantity', 'number_of_imaging_drums'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cartridges::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'accepted_id' => $this->accepted_id,
            'fulfilled_id' => $this->fulfilled_id,
            'accepted_date' => $this->accepted_date,
            'job_id' => $this->job_id,
            'full_name_id' => $this->full_name_id,
            'cartridge_id' => $this->cartridge_id,
            'toner_id' => $this->toner_id,
            'drum_id' => $this->drum_id,
        ]);

        $query->andFilterWhere(['like', 'given_away', $this->given_away])
            ->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'sum', $this->sum])
            ->andFilterWhere(['like', 'payment', $this->payment])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'toner_quantity', $this->toner_quantity])
            ->andFilterWhere(['like', 'number_of_imaging_drums', $this->number_of_imaging_drums]);

        $query->joinWith(['full_name', 'customer']);
        $query
            ->orFilterWhere(['like', 'full_name.name', $this->search])
            ->orFilterWhere(['like', 'customer.name', $this->search]);

        return $dataProvider;
    }
}
