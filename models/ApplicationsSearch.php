<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Applications;

/**
 * ApplicationsSearch represents the model behind the search form about `app\models\Applications`.
 */
class ApplicationsSearch extends Applications
{
    public $search;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'full_name_id', 'accepted_id', 'fulfilled_id'], 'integer'],
            [['status','search', 'customer_id', 'phone', 'request', 'date_of_adoption', 'date_of_completion', 'date_of_payment', 'comment', 'sum', 'payment', 'telephone', 'material_price', 'price_of_work'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Applications::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'full_name_id' => $this->full_name_id,
            'date_of_adoption' => $this->date_of_adoption,
            'date_of_completion' => $this->date_of_completion,
            'date_of_payment' => $this->date_of_payment,
            'accepted_id' => $this->accepted_id,
            'fulfilled_id' => $this->fulfilled_id,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'customer_id', $this->customer_id])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'request', $this->request])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'sum', $this->sum])
            ->andFilterWhere(['like', 'payment', $this->payment])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'material_price', $this->material_price])
            ->andFilterWhere(['like', 'price_of_work', $this->price_of_work]);

        $query->joinWith(['full_name', 'customer']);
        $query
            ->orFilterWhere(['like', 'full_name.name', $this->search])
            ->orFilterWhere(['like', 'customer.name', $this->search])
            ->orFilterWhere(['like', 'request', $this->search]);
        return $dataProvider;
    }
}
