<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "full_name".
 *
 * @property int $id
 * @property string $name Ф.И.О.
 */
class FullName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'full_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Ф.И.О.',
        ];
    }

    /**
     * @inheritdoc
     * @return FullNameQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FullNameQuery(get_called_class());
    }
}
