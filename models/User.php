<?php 
namespace app\models;

use app\helpers\TagHelper;
use http\Exception;
use SendGrid\Mail\Mail;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $name
 * @property string $password_hash
 * @property string $password
 * @property string $password_open
 * @property integer $status
 * @property integer $is_deletable
 * @property string $phone
 * @property string $role
 * @property string $avatar
 * @property string $candidate_id
 *
 *
 * @property User $identity
 */
class User extends ActiveRecord implements IdentityInterface
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    const ROLE_ADMIN = 0;

    public $password;

    private $oldPasswordHash;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['name', 'login', 'is_deletable', 'password', 'password_hash', 'phone', 'role'],
            self::SCENARIO_EDIT => ['name', 'login', 'is_deletable', 'password', 'password_hash', 'timezone', 'phone', 'role'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['password'], 'required', 'on' => self::SCENARIO_DEFAULT],
//            ['login', 'match', 'pattern' => '/^[a-z]+([-_]?[a-z0-9]+){0,2}$/i', 'message' => '{attribute} должен состоять только из латинских букв и цифр'],
//            ['password', 'match', 'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,50}$/', 'message' => '{attribute} не соответствует всем параметрам безопасности'],
            [['login'], 'unique'],
            [['is_deletable'], 'integer'],
            [['login', 'password_hash', 'password', 'name', 'phone', 'avatar', 'password_open'], 'string', 'max' => 255],
        ];
    }



    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        $uid = Yii::$app->user->identity->id;

        if($uid == $this->id)
        {
            Yii::$app->session->setFlash('error', "Вы авторизованы под пользователем «{$this->login}». Удаление невозможно!");
            return false;
        }

        if($this->is_deletable == false)
        {
            Yii::$app->session->setFlash('error', "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }

    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->role === self::ROLE_ADMIN;
    }

    /**
     * @return bool
     */
    public function isAdministration()
    {
        /** @var User $identity */
        $identity = Yii::$app->user->identity;

        return $identity->role === self::ROLE_ADMIN;
    }

    /**
     * @return bool
     */
    public function isManager()
    {
        return Yii::$app->user->identity->role === self::ROLE_MANAGER;
    }

    /**
     * @return bool
     */
    public function isLimitedManager()
    {
        return Yii::$app->user->identity->role === self::ROLE_LIMITED_MANAGER;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($this->password != null){
                Yii::info('Пароль перед сохранением: ' . $this->password, 'test');
                
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'name' => 'Ф.И.О.',
            'password_hash' => 'Password Hash',
            'password_open' => 'Password',
            'password' => 'Пароль',
            'status' => 'Статус',
            'is_deletable' => 'Deletable',
            'phone' => 'Телефон',
            'avatar' => 'Avatar',
            'role' => 'Роль',

        ];
    }

    /**
     * @return array
     */
    public static function roleLabels()
    {
        return [
            self::ROLE_ADMIN => 'Admin',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getPermmission()
    {
        return $this->stat_indet;
    }

    /**
     * @return string
     */
    public function getRealAvatarPath()
    {
        return $this->avatar != null ? $this->avatar : 'img/nouser.png';
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public static function getManagerList()
    {
        $query = self::find()->andWhere(['role' => self::ROLE_MANAGER]);

        return ArrayHelper::map($query->all(), 'id', 'name');
    }
}
