<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TypeToTech]].
 *
 * @see TypeToTech
 */
class TypeToTechQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TypeToTech[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TypeToTech|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
