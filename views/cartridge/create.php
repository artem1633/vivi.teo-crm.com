<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Cartridge */

?>
<div class="cartridge-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
