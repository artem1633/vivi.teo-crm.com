<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FullName */

?>
<div class="full-name-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
