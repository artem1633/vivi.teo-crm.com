<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Photoconductor */

?>
<div class="photoconductor-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
