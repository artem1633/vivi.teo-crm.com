<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Photoconductor */
?>
<div class="photoconductor-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
