<?php

use yii\helpers\Url;


?>



<div id="top-menu" class="top-menu" style="margin-top: -16px;">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
            echo \app\admintheme\widgets\TopMenu::widget(
                [
                    'options' => ['class' => 'nav'],
                    'items' => [
                        ['label' => 'Заявки', 'icon' => 'fa fa-book', 'url' => ['/applications']],
                        ['label' => 'Картриджи ', 'icon' => 'fa fa-print', 'url' => ['/cartridges']],
                        ['label' => 'Техника', 'icon' => 'fa fa-hdd-o', 'url' => ['/technics']],


                        ['label' => 'Справочники', 'icon' => 'fa fa-book', 'url' => '/customer', 'options' => ['class' => 'has-sub'],
                            'items' => [
                                ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'], ],
                                ['label' => 'Клиент', 'icon' => 'fa  fa-users', 'url' => ['/customer']],
                                ['label' => 'ФИО', 'icon' => 'fa  fa-users', 'url' => ['/full-name']],
                                ['label' => 'Список техники', 'icon' => 'fa fa-object-ungroup', 'url' => ['/technics-type']],
                                ['label' => 'Тонер', 'icon' => 'fa fa-tint', 'url' => ['/toner']],
                                ['label' => 'Фотобарабан', 'icon' => 'fa fa-tags', 'url' => ['/photoconductor']],
                                ['label' => 'Работы', 'icon' => 'fa fa-gavel', 'url' => ['/job']],
                                ['label' => 'Картридж', 'icon' => 'fa fa-eyedropper', 'url' => ['/cartridge']],
                                ['label' => 'Материалы', 'icon' => 'fa fa-cubes', 'url' => ['/materials']],

//
                            ]],
                    ],
                ]
            );
        ?>
    <?php endif; ?>
</div>
