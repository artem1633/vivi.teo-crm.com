<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Toner */

?>
<div class="toner-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
