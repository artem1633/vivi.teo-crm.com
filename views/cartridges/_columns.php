<?php

use app\models\Customer;
use app\models\FullName;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute'=>'id',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'filter' => [
            'Принят' => 'Принят',
            'Готов' => 'Готов',
            'Отдан' => 'Отдан',
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(Customer::find()->where(['id' => $data->customer_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => false
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'accepted_date',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'job_id',
        'value'     => function ($model) {
            if ($model->job_id != null) {
                return ArrayHelper::getValue(\app\models\Job::find()->where(['id' => $model->job_id])->one(),'name');
            } else {
                return '';
            }
        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'full_name_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(FullName::find()->where(['id' => $data->full_name_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'accepted_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(\app\models\User::find()->where(['id' => $data->accepted_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => false
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fulfilled_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(\app\models\User::find()->where(['id' => $data->fulfilled_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => false
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cartridge_id',
        'value' => function ($model) {
            if ($model->cartridge_id != null) {
                return ArrayHelper::getValue(\app\models\Cartridge::find()->where(['id' => $model->cartridge_id])->one(),'name');
            } else {
                return '';
            }
        },
        'filter' => false
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comments',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sum',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'payment',
        'filter' => false
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Удаление',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],

];   