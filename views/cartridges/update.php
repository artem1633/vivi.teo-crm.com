<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Cartridges */
?>
<div class="cartridges-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
