<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model Cartridges */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="cartridges-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList([
                    'Принят' => 'Принят',
                    'Готов' => 'Готов',
                    'Отдан' => 'Отдан',
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'customer_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(\app\models\Customer::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выберите'
                ],
            ]) ?>
        </div>
    </div>


    

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'accepted_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите',
                        'value' => Yii::$app->user->identity->id,
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'fulfilled_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
            </div>
        </div>
    


    

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'job_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Job::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                    'pluginEvents' => [
                        "change:select" => "function() {
                        let cartVal = document.getElementById('cartridges-cartridge_id').value;
                        console.log(cartVal);
                            $.ajax({
                                method: 'GET',
                                url: '/cartridges/search-job?q='+$(this).val()+'&cart='+cartVal,
                                success: function(response){

                                console.log(response);
                                document.getElementById('cartridges-sum').value = response;

                                }
                            });
                        }",
                        "change" => "function() {
                        let cartVal = document.getElementById('cartridges-cartridge_id').value;
                            $.ajax({
                                method: 'GET',
                                url: '/cartridges/search-job?q='+$(this).val()+'&cart='+cartVal,
                                success: function(response){

                             
                                    console.log(response);
                                    document.getElementById('cartridges-sum').value = response;

                                }
                            });
                        }",
                    ],
                ]) ?>
            </div>
        </div>
    

    

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'full_name_id')->widget(kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\FullName::find()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Выберите '],
                    'pluginOptions' => [
                        'tags' => true,
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'cartridge_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Cartridge::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                    'pluginEvents' => [
                        "change:select" => "function() {
                        let jobVal = document.getElementById('cartridges-job_id').value;
                        console.log(cartVal);
                            $.ajax({
                                method: 'GET',
                                url: '/cartridges/search-cartrige?q='+$(this).val()+'&job='+jobVal,
                                success: function(response){

                                    console.log(response);
                                document.getElementById('cartridges-sum').value = response;
                                }
                            });
                        }",
                        "change" => "function() {
                         let jobVal = document.getElementById('cartridges-job_id').value;
                            $.ajax({
                                method: 'GET',
                                url: '/cartridges/search-cartrige?q='+$(this).val()+'&job='+jobVal,
                                success: function(response){

                                    console.log(response);

                                    document.getElementById('cartridges-sum').value = response;


                                }
                            });
                        }",
                    ],
                ]) ?>
            </div>
        </div>
    

        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'comments')->textarea(['rows' => 6]) ?>
            </div>
        </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'toner_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(\app\models\Toner::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выберите'
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'toner_quantity')->textInput(['value' => 1]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'drum_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(\app\models\Photoconductor::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выберите'
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'number_of_imaging_drums')->textInput(['value' => 1]) ?>
        </div>
    </div>
    

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'payment')->dropDownList([
                        'Нал' => 'Нал',
                        'Безнал' => 'Безнал',
                        'Kaspi' => 'Kaspi'
                ]) ?>
            </div>
            <div class="col-md-6">
                    <?= $form->field($model, 'sum')->textInput() ?>
            </div>
        </div>

    

    


    


    


    

    

    


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
