<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Cartridges */

?>
<div class="cartridges-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
