<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin(); ?>


        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'name')->textInput() ?>
            </div>
        </div>
    

        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'bin')->textInput() ?>
            </div>
        </div>
    

        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'telephone')->textInput() ?>
            </div>
        </div>
    

        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'contract')->textInput() ?>
            </div>
        </div>
    

        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'payment')->textInput() ?>
            </div>
        </div>
    

        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'valid_to')->widget(
                        DateTimePicker::className(),[
                            'options' => ['placeholder' => 'Выберите дату  ...'],
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true
                            ]
                        ]
                    ) ?>
            </div>
        </div>
    

        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'black_list')->textInput() ?>
            </div>
        </div>
    

        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'comment')->textInput() ?>
            </div>
        </div>
    


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
