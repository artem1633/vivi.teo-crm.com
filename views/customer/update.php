<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Customer */
?>
<div class="customer-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
