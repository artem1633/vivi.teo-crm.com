<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TechnicsType */

?>
<div class="technics-type-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
