<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Applications */

?>
<div class="applications-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
