<?php

use app\models\Customer;
use app\models\FullName;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

return [

    [
        'class' => 'kartik\grid\DataColumn',
        'attribute'=>'id',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'filter' => [
            'Принят' => 'Принят',
            'В работе' => 'В работе',
            'Ожидает оплаты' => 'Ожидает оплаты',
            'Готово' => 'Готово',
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(Customer::find()->where(['id' => $data->customer_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => false

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'full_name_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(FullName::find()->where(['id' => $data->full_name_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => false

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'request',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_of_adoption',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_of_completion',
        'value'     => function ($model) {
            if ($model->date_of_completion != null) {
                return $model->date_of_completion;
            } else {
                return '';
            }
        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_of_payment',
        'value'     => function ($model) {
            if ($model->date_of_payment != null) {
                return $model->date_of_payment;
            } else {
                return '';
            }
        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'accepted_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(\app\models\User::find()->where(['id' => $data->accepted_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => false

    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fulfilled_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(\app\models\User::find()->where(['id' => $data->fulfilled_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => false
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sum',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'payment',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Действия',
        'content' => function($model){
            $update = Html::a('<i class="fa fa-pencil" style="font-size: 16px;"></i>', ['applications/update', 'id' => $model->id],
                ['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip']
            );
            $delete = Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['applications/delete', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'],[
                'role'=>'modal-remote','title'=>'Удалить',
                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                'data-request-method'=>'post',
                'data-toggle'=>'tooltip',
                'data-confirm-title'=>'Вы уверены?',
                'data-confirm-message'=>'Вы действительно хотите удалить файл',
                'style' => 'margin-left: 10px;',
            ]);
           if ($model->status == 'Принят'){
              $toWork =  Html::a('<i class="fa fa-cogs text-success" style="font-size: 16px;"></i>', ['applications/to-work', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'],[
                  'role'=>'modal-remote','title'=>'в работу',
                  'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                  'data-request-method'=>'post',
                  'data-toggle'=>'tooltip',
                  'data-confirm-title'=>'Вы уверены?',
                  'data-confirm-message'=>'Вы действительно хотите перевести заявку в работу',
                  'style' => 'margin-left: 10px;',
              ]);
              return $update . $toWork . $delete;

           }elseif ($model->status == 'В работе'){
               $toPay =  Html::a('<i class="fa fa-usd text-success" style="font-size: 16px;"></i>', ['applications/to-pay', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'],[
                   'role'=>'modal-remote','title'=>'в оплату',
                   'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                   'data-request-method'=>'post',
                   'data-toggle'=>'tooltip',
                   'data-confirm-title'=>'Вы уверены?',
                   'data-confirm-message'=>'Вы действительно хотите перевести заявку в статус ожидает оплаты?',
                   'style' => 'margin-left: 10px;',
               ]);
               return $update . $toPay . $delete;

           }elseif ($model->status == 'Ожидает оплаты'){
               $toSuccess =  Html::a('<i class="fa fa-check-square-o text-success" style="font-size: 16px;"></i>', ['applications/to-success', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'],[
                   'role'=>'modal-remote','title'=>'готово',
                   'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                   'data-request-method'=>'post',
                   'data-toggle'=>'tooltip',
                   'data-confirm-title'=>'Вы уверены?',
                   'data-confirm-message'=>'Вы действительно хотите перевести заявку в готово?',
                   'style' => 'margin-left: 10px;',
               ]);
               return $update . $toSuccess . $delete;
           }else{
               return $update . $delete;
           }
        }
    ],

//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//                return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
//                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                          'data-request-method'=>'post',
//                          'data-toggle'=>'tooltip',
//                          'data-confirm-title'=>'Are you sure?',
//                          'data-confirm-message'=>'Are you sure want to delete this item'],
//    ],

];   