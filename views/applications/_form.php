<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
$matPrice = <<<JS
        let matVal = $(this).val();
        let workVal = document.getElementById("applications-price_of_work").value;
        let sum = Number(matVal) + Number(workVal);
        document.getElementById("applications-sum").value = sum;
        
JS;
$workPrice = <<<JS
        let workVal= $(this).val();
        let matVal = document.getElementById("applications-material_price").value;
        let sum = Number(matVal) + Number(workVal);
        document.getElementById("applications-sum").value = sum;
     
JS;
?>

<div class="applications-form">

    <?php $form = ActiveForm::begin(); ?>


        <div class="row">
            <div class="col-md-6">
                    <?= $form->field($model, 'status')->dropDownList([
                            'Принят' => 'Принят',
                            'В работе' => 'В работе',
                            'Ожидает оплаты' => 'Ожидает оплаты',
                            'Готово' => 'Готово',
                    ])?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'customer_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Customer::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
            </div>
        </div>

    

        <div class="row">
            <div class="col-md-6">

                    <?= $form->field($model, 'full_name_id')->widget(kartik\select2\Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\FullName::find()->all(), 'id', 'name'),
                        'options' => ['placeholder' => 'Выберите '],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ]);
                    ?>

            </div>
            <div class="col-md-6">
                <?=
                $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+7 (999)-999-99-99'
                ]);
                ?>
            </div>
        </div>
    

        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'request')->textarea(['rows' => 6]) ?>
            </div>
        </div>



        <div class="row">
            <div class="col-md-6">
                    <?= $form->field($model, 'accepted_id')->widget(Select2::class, [
                        'data' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
                        'options' => [
                            'placeholder' => 'Выберите',
                            'value' => Yii::$app->user->identity->id,
                        ],
                    ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'fulfilled_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
            </div>
        </div>
    

        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
            </div>
        </div>


        <div class="row">
            <div class="col-md-3">
                    <?= $form->field($model, 'payment')->dropDownList([
                            'Безнал' => 'Безнал',
                            'Нал' => 'Нал',
                            'Kaspi' => 'Kaspi'
                    ])?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'material_price')->textInput([
                    'onChange' => $matPrice
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'price_of_work')->textInput([
                    'onChange' => $workPrice
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'sum')->textInput() ?>
            </div>
        </div>
    


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
