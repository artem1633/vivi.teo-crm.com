<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Applications */
?>
<div class="applications-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
