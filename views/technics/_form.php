<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model Technics */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false) {
    $model->list = ArrayHelper::getColumn(\app\models\TypeToTech::find()->where(['technics_id' => $model->id])->all(), 'technics_type_id');
    $model->list = ArrayHelper::getColumn(\app\models\TechnicsType::find()->where(['id' => $model->list])->all(), 'id');

}

$matPrice = <<<JS
        let matVal = $(this).val();
        let workVal = document.getElementById("technics-price_of_work").value;
        let sum = Number(matVal) + Number(workVal);
        document.getElementById("technics-sum").value = sum;
        
JS;
$workPrice = <<<JS
        let workVal= $(this).val();
        let matVal = document.getElementById("technics-material_price").value;
        let sum = Number(matVal) + Number(workVal);
        document.getElementById("technics-sum").value = sum;
     
JS;
?>

<div class="technics-form">

    <?php $form = ActiveForm::begin(); ?>


        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'status')->dropDownList([
                    'Принят' => 'Принят',
                    'Диагностика' => 'Диагностика',
                    'Согласование' => 'Согласование',
                    'В работе' => 'В работе',
                    'Готов' => 'Готов',
                    'Отдан' => 'Отдан',
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'customer_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Customer::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
            </div>
        </div>

    

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'full_name_id')->widget(kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\FullName::find()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Выберите '],
                    'pluginOptions' => [
                        'tags' => true,
                        'allowClear' => true,
                    ],
                ]);
                ?>

            </div>
            <div class="col-md-6">
                <?=
                $form->field($model, 'telephone')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+7 (999)-999-99-99'
                ]);
                ?>
            </div>
        </div>
    

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'accepted_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите',
                        'value' => Yii::$app->user->identity->id,
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'executor_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
            </div>
        </div>
    


    

        <div class="row">
            <div class="col-md-12">

            </div>
        </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'list')->widget(Select2::className(), [
                'data' => ArrayHelper::map(\app\models\TechnicsType::find()->all(), 'id', 'name'),
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'tags' => false,
                    'tokenSeparators' => [','],
                ], 'pluginEvents' => [
                    "change" => "function() {
                       let tmp = $(this).val();
                       let str = '';
                       $.post(
                       '/technics/find-last-number',function(data){
                       for(var i = 1;i < tmp.length+1;i++){
                       str += String((Number(data)+i))+',';
                       $('#technics-numbers').val(str.slice(0,-1));
                       }
                       }
                       )
                        }",
                ]

            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'numbers')->textInput() ?>
        </div>
    </div>


        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'problems')->textarea(['rows' => 6]) ?>
            </div>
        </div>
    


    

        <div class="row">
            <div class="col-md-12">
                    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
            </div>
        </div>
    

    

        <div class="row">
            <div class="col-md-3">
                    <?= $form->field($model, 'payment')->dropDownList([
                        'Нал' => 'Нал',
                        'Безнал' => 'Безнал',
                    ]) ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'material_price')->textInput([
                    'onChange' => $matPrice
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'price_of_work')->textInput([
                    'onChange' => $workPrice
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'sum')->textInput() ?>
            </div>
        </div>
    


    


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
