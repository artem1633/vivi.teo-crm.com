<?php

use app\models\Customer;
use app\models\FullName;
use app\models\TechnicsType;
use app\models\TypeToTech;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute'=>'id',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'filter' => [
            'Принят' => 'Принят',
            'Диагностика' => 'Диагностика',
            'Согласование' => 'Согласование',
            'В работе' => 'В работе',
            'Готов' => 'Готов',
            'Отдан' => 'Отдан',
        ]
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(Customer::find()->where(['id' => $data->customer_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'full_name_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(FullName::find()->where(['id' => $data->full_name_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'accepted_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(\app\models\User::find()->where(['id' => $data->accepted_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => false
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'executor_id',
        'content' => function($data){
            $customer = ArrayHelper::getColumn(\app\models\User::find()->where(['id' => $data->executor_id])->all(), 'name');
            return implode('',$customer);

        },
        'filter' => false
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'problems',
        'filter' => false
    ],
    [
        'attribute' => 'list',
        'value' => function($data){
            $exec = ArrayHelper::getColumn(TypeToTech::find()->where(['technics_id' => $data->id])->all(), 'technics_type_id');
            $exec = ArrayHelper::getColumn(TechnicsType::find()->where(['id' => $exec])->all(), 'name');

            return implode(' ,',$exec);

        },
        'filter' => false,
        'width' => '10%',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sum',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'payment',
        'filter' => false
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   