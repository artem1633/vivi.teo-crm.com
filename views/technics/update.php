<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Technics */
?>
<div class="technics-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
