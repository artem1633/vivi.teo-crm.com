<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Technics */

?>
<div class="technics-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
