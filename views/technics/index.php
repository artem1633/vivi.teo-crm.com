<?php

use kartik\export\ExportMenu;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel TechnicsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "техника";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse project-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title">техника</h4>
    </div>
    <div class="panel-body">
<div class="technics-index">
    <div class="row">
        <div class="col-md-2">
            <?php $form = ActiveForm::begin(['id' => 'search-form', 'method' => 'GET']); ?>
            <?= $form->field($searchModel, 'search')->textInput()->label(false) ?>

        </div>
        <div class="col-md-3">
            <?= Html::a('Сбросить', ['/technics/index'], ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton('Применить', ['class' => 'btn btn-success']) ?>

            <?php ActiveForm::end() ?>
        </div>
    </div>
    <div class="row">
        <?php
        $download = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => require(__DIR__.'/_columns.php'),
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Скачать',
                'class' => 'btn btn-success'
            ],
            'showColumnSelector' => false,
            'clearBuffers' => true,
            'exportConfig' => [
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_EXCEL => false,
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_PDF => false,
            ],
            'showConfirmAlert' => false,
        ]);
        ?>
    </div>
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                ['role'=>'modal-remote','title'=> 'Добавить проект','class'=>'btn btn-success']).'&nbsp;'.
                Html::a('<i class="fa fa-repeat"></i>', [''],
                ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']). '<span style="margin-left:10px">'.$download.'</span>' ,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'headingOptions' => ['style' => 'display: none;'],
//                'after'=>BulkButtonWidget::widget([
//                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
//                                ["bulk-delete"] ,
//                                [
//                                    "class"=>"btn btn-danger btn-xs",
//                                    'role'=>'modal-remote-bulk',
//                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                                    'data-request-method'=>'post',
//                                    'data-confirm-title'=>'Are you sure?',
//                                    'data-confirm-message'=>'Вы действительно хотите удалить эту запись?'
//                                ]),
//                        ]).
//                        '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
